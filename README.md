# Shopping List (WoW Vanilla 1.12)

It will have a db of recipes for all profession, so that after a quick set up, you will be able to quickly see what mats you still need to farm/buy.

The idea here is to make it easier to organise your raid consumables farm.

There will only be 1 shopping list.

## Setup

You will need to set it up first.

Select what consumables you want, and in what quantity.

This information will be saved in localStorage, so it's device based.

## Shopping List

Here you will see all the basic mats you need to be able to create all your consumables. This list will include vials, dyes, threads etc.

## Adjusting quantities

That's the part which is not yet worked out. There must be a nice UX for it, but I can't figure it out. It will come to me one day.

### It's quite a simple project, but if you enjoy it, send some love :-)