self.addEventListener('install', function(event) {
  var CACHE_NAME = 'my-site-cache-v2';
  var urlsToCache = [
    'assets/recipes.json',
    'assets/scripts.js',
    'assets/style.css',
    'index.html',
    'manifest.json'
  ];
  
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});