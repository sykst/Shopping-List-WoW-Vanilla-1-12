if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('sw.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}

var requestURL = 'assets/recipes.json';
var request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function() {
    recipesFile = request.response;
    showRecipes(recipesFile);
    showMats();
};
setupLoad();

function showRecipes(jsonObj) {
	var section = document.getElementById("recipesList");

    // Get selection from localStorage if it exists
    var userSelection = localStorage.getItem('sykst-shopList-userSelection');

    if (userSelection !== null) {
        var selection = JSON.parse(userSelection);
    }

	for (var i = 0; i < jsonObj.length; i++) {
		var recipeName = document.createElement('label');
		var recipeCount = document.createElement('input');
		recipeCount.type = "number";
		recipeCount.step = "1";
		recipeCount.min = "0";
        recipeCount.placeholder = "0";
        recipeCount.name = jsonObj[i]["name"];

        // Get selection from localStorage if it exists
        if (userSelection !== null) {

            for (var j = 0; j < Object.keys(selection).length; j++) {
                if ( recipeCount.name === selection[j][0]) {
                    recipeCount.value = selection[j][1];
                }
            }

        }

		recipeName.textContent = jsonObj[i]["name"];
		recipeName.appendChild(recipeCount);
		section.appendChild(recipeName);
	}
}

function saveSelection() {
    var selection = {};
    var inputBoxes = document.querySelectorAll('input')
    for (var i = 0; i < inputBoxes.length; i ++) {
        selection[i] = [inputBoxes[i].name, inputBoxes[i].value]
    }

    localStorage.setItem('sykst-shopList-userSelection', JSON.stringify(selection));
    var saveSuccess = document.getElementById("saveSuccess");
    saveSuccess.classList.remove("hidden");
    setTimeout(function() {
        saveSuccess.classList.add("hidden"); 
    }, 5000);

    showMats();
}

function showMats() {
    var userSelection = localStorage.getItem('sykst-shopList-userSelection');
    var section = document.getElementById('matsList');

    // removes all children
    while (section.lastChild) {
        section.removeChild(section.lastChild);
    }

    // Mats available
    var possibleMats = [
        ['Bruiseweed', 0],
        ['Dense Stone', 0],
        ['Wild Steelbloom', 0],
        ['Goldthorn', 0],
        ['Sungrass', 0],
        ['Mountain Silversage', 0],
        ['Plaguebloom', 0],
        ['Stranglekelp', 0],
        ['Oily Blackmouth', 0],
        ['Dream Dust', 0],
        ['Dreamfoil', 0],
        ['Elemental Fire', 0],
        ['Blindweed', 0],
        ['Ghost Mushroom', 0],
        ['Golden Sansam', 0],
        ['Liferoot', 0],
        ['Elemental Earth', 0],
        ['Swiftthistle', 0],
        ['Gromsblood', 0],
        ['Stonescale Eel', 0],
        ['Black Lotus', 0],
        ['Shardtooth E\'ko', 0],
        ['Frostsaber E\'ko', 0],
        ['Frostmaul E\'ko', 0],
        ['Winterfall E\'ko', 0],
        ['Rumsey Rum Black', 0],
        ['Stratholme Holy Water', 0],
        ['Leaded Vial', 0],
        ['Crystal Vial', 0],
        ['Empty Vial', 0],
        ['Refreshing Spring Water', 0]
    ]

    if (userSelection === null) {

        var firstTime = document.createElement('p');
        firstTime.textContent = "Pick your consumables to see your shopping list!";
        section.appendChild(firstTime);

    } else {

        var selection = JSON.parse(userSelection);

        for (var i = 0; i < Object.keys(selection).length; i++) {

            for (var j = 0; j < recipesFile.length; j++){

                if (recipesFile[j].name == selection[i][0]){
                    
                    for (var k = 0; k < recipesFile[j].mats.length; k++) {

                        var matName = recipesFile[j].mats[k].name;
                        var matQuantity = recipesFile[j].mats[k].quantity;
                        
                        for (var z = 0; z < possibleMats.length; z++) {

                            if (possibleMats[z].indexOf(matName) !== -1) {
                                possibleMats[z][1] = parseInt(possibleMats[z][1]) + parseInt(matQuantity)*parseInt(selection[i][1]);
                            }

                        }

                    }

                }

            }

        }

        // Print the list
        for (var i = 0; i < possibleMats.length; i++) {
            if (possibleMats[i][1] > 0) {
                var listOuter = document.createElement('tr');
                var listMat = document.createElement('td');
                var listNum = document.createElement('td');

                listMat.textContent = possibleMats[i][0];
                listNum.textContent = possibleMats[i][1];

                listOuter.appendChild(listMat);
                listOuter.appendChild(listNum);
                section.appendChild(listOuter);
            }
        }

    }
}

function setupLoad() {
    var userSelection = localStorage.getItem('sykst-shopList-hideSetup');
    setup = document.getElementById("setup");

    if (userSelection === "true") {
        setup.classList.add("hidden");
    }
}

function toggleSetup() {
    setup = document.getElementById("setup");
    setup.classList.toggle("hidden");
    if ( setup.classList.contains("hidden") ) {
        localStorage.setItem('sykst-shopList-hideSetup', true);
    } else {
        localStorage.setItem('sykst-shopList-hideSetup', false);
    }
}